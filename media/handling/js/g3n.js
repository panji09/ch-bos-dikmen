$(document).ready(function(){
	
	$( "#dialog" ).dialog(
		{ 
			autoOpen: false, 
			height: 80,
      		
			
		}
	);

	
	//alert('some mind');
	
	$('#Rekam').click(function(){
		$('#InputForm').dialog('open');
		ResetModul();
		Rekam();
	});
	
	$('#Cancel').click(function(){
		$('#InputForm').dialog('close');
	});
	
	$('#Simpan').click(function(){
		SaveModul();
		$('#InputForm').dialog('close');
		ResetModul();
	});
	
	Startup();
	
	LoadData();
	
});

// inspira talenta function, not mine
function formatdecimal(sep, dec, obj) {

	var split = obj.value.split(dec);
	var beforedec = split[0].replace(/[\Wa-zA-Z]/g,'');
	var afterdec = split[1];

	var counter = 0;
	var arrcounter = 0;
	var arr = new Array();
	var result = '';

	for (var i=beforedec.length-1 ; i >=0 ; i--)
	{
		counter++;

		if (counter == 3)
		{
			arr[arrcounter] = beforedec.substr(i,3);
			counter = 0;
			arrcounter++;
		}

		if (i == 0)
		{
			if (beforedec.substr(i,counter).length > 0)
			arr[arrcounter] = beforedec.substr(i,counter);
		}
	}

	for (var i in arr)
	{
		if (i == 0)
			result = arr[i] + result;
		else
			result = arr[i] + sep + result;
	}

	if (split.length > 1)
	{
		afterdec = afterdec.replace(/[\Wa-zA-Z]/g,'');
		obj.value = result + dec + afterdec;
	}
	else
		obj.value = result;
}

function fixLength(o,max) {
	 var tempstr, tempint;
	 if ( o.value.length > max ) {
		o.value = o.value.substring(0, max);
	 }
}

function btn_showhide(param,tujuan){
	if($(param).val()=='hide'){
		$(tujuan).show('slow');
		$(param).val('show');
	} else {
		$(tujuan).hide('slow');
		$(param).val('hide');
	}
}

function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '') return true;
    if (typeof obj == 'number' && isNaN(obj)) return true;
    if (obj instanceof Date && isNaN(Number(obj))) return true;
    return false;
}
