<?php $this->session->set_userdata('redirect','admin_handling/halaman/'.$this->uri->segment(3));?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <header class="dark">
                            <div class="icons">
                                <i class="fa fa-ok"></i>
                            </div>
                            <h5><?=$title?></h5>
                            <div class="toolbar">
                              <ul class="nav">
                                <li>
                                  <div class="btn-group">
                                    <a class="accordion-toggle btn btn-xs minimize-box" data-toggle="collapse" href="#collapse2">
                                      <i class="fa fa-chevron-up"></i>
                                    </a>
                                  </div>
                                </li>
                              </ul>
                            </div>
                        </header>
                        <div id="collapse2" class="body collapse in">
                            <form id='form_content' role="form" action='<?=site_url('admin_handling/halaman/post_content_dinamis/'.$content_kategori_id.'/'.$id)?>' method='post'>
                                <div class="form-group">
                                  <label for="">Judul</label>
                                  <input type="text" name='title' class="form-control" id="" placeholder="Judul" value='<?=$dinamis->title?>'>
                                </div>
                                <div class="form-group">
                                  <label for="">Isi</label>
                                  <textarea class='form-control ckeditor' name='content' placeholder='Isi'><?=$dinamis->content?></textarea>
                                </div>
                                
                                <div class="checkbox">
                                  <label>
                                    <input type="checkbox" name='published' value='1' <?=($dinamis->published ? 'checked' : '')?>> Terbitkan
                                  </label>
                                </div>
                                <div id='hide_input' style='display:none'>
                                </div>
                                <button type="submit" id='submit' class="btn btn-default">Submit</button>
                            </form>
                            <div>&nbsp</div>
                            <form role='form' id="fileupload" data-url="<?=site_url('admin_handling/halaman/post_upload')?>" method="POST" enctype="multipart/form-data">
				<div class="form-group">
				  <!-- Redirect browsers with JavaScript disabled to the origin page -->
				  <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
				  <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
				  <div class="row fileupload-buttonbar">
				      <div class="col-lg-7">
					  <!-- The fileinput-button span is used to style the file input field as button -->
					  <span class="btn btn-success fileinput-button">
					      <i class="glyphicon glyphicon-plus"></i>
					      <span>Add files...</span>
					      <input type="file" name="files[]" multiple>
					  </span>
					  <button type="submit" class="btn btn-primary start">
					      <i class="glyphicon glyphicon-upload"></i>
					      <span>Start upload</span>
					  </button>
					  <button type="reset" class="btn btn-warning cancel">
					      <i class="glyphicon glyphicon-ban-circle"></i>
					      <span>Cancel upload</span>
					  </button>
					  <button type="button" class="btn btn-danger delete">
					      <i class="glyphicon glyphicon-trash"></i>
					      <span>Delete</span>
					  </button>
					  <input type="checkbox" class="toggle">
					  <!-- The global file processing state -->
					  <span class="fileupload-process"></span>
				      </div>
				      <!-- The global progress state -->
				      <div class="col-lg-5 fileupload-progress fade">
					  <!-- The global progress bar -->
					  <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
					      <div class="progress-bar progress-bar-success" style="width:0%;"></div>
					  </div>
					  <!-- The extended global progress state -->
					  <div class="progress-extended">&nbsp;</div>
				      </div>
				  </div>
				  <!-- The table listing the files available for upload/download -->
				  <table role="presentation" class="table table-striped">
				    <tbody class="files">
					<?php if($id):?>
					<?php
					    $files=$this->content_files_db->get($id);
					    
					    if($files->num_rows())
					    foreach($files->result() as $row):
					?>
					
					<tr class="template-download fade in">
					    <td>
						<span class="preview">
						    
						</span>
					    </td>
					    <td>
						<p class="name"> 
							<a download="<?=$row->file?>" title="<?=$row->file?>" href="<?=base_url('media/upload/files/'.$row->file)?>"><?=$row->file?></a>
						</p>
						
						    <p class="name">
						      <input type="text" value="<?=$row->file?>" class="form-control name_link">
						      <input type="hidden" value="<?=$row->name?>" class="form-control name_file">
						      <input type="hidden" value="<?=$row->size?>" class="form-control size_file">
						    </p>
						
					    </td>
					    <td>
						<span class="size"><?=$row->size?></span>
					    </td>
					    <td>
						
						    <button data-url="<?=site_url('/admin_handling/halaman/post_upload?file='.$row->file).'&id='.$row->id?>" data-type="DELETE" class="btn btn-danger delete">
							<i class="glyphicon glyphicon-trash"></i>
							<span>Delete</span>
						    </button>
						    <input type="checkbox" class="toggle" value="1" name="delete">
						
					    </td>
					</tr>
					<?php endforeach;?>
					<?php endif;?>
				    </tbody>
				  </table>
                                </div>
                                
				
			    </form>
                        </div>
                    </div>
                </div>
            </div>