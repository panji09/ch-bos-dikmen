    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.html">Home</a></h6>
                    <h6><span class="page-active">FAQ (Frequenly Asked Question)</span></h6>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="row">
	    <!-- Here begin Main Content -->
	    <div class="col-md-12">
		
		<div class="row">
		    <div class="col-md-12">
			<div id="blog-comments" class="blog-post-container">
			    <div class="blog-post-inner">
				<div class="panel-group" id="accordion">
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1">
                                          Pertanyaan 1
                                        </a>
                                    </div>
                                    <div id="collapse-1" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        Jawaban 1
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-2">
                                          Pertanyaan 2
                                        </a>
                                    </div>
                                    <div id="collapse-2" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        Jawaban 2
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-3">
                                          Pertanyaan 3
                                        </a>
                                    </div>
                                    <div id="collapse-3" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        jawaban 3
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div id="sharethis">
				    <span class='st_facebook_vcount' displayText='Facebook'></span>
				    <span class='st_twitter_vcount' displayText='Tweet'></span>
				    <span class='st_googleplus_vcount' displayText='Google +'></span>
				    <span class='st_email_vcount' displayText='Email'></span>
				</div>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
