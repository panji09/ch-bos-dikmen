    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="footer-widget">
                        <h4 class="footer-widget-title">Hubungi Kami</h4>
                        <p>Direktorat Pembinaan Sekolah Menengah Atas - Kementerian Pendidikan dan Kebudayaan <br>Jalan R.S. Fatmawati, Cipete - Kode pos 12410, Jakarta Selatan</p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-widget">
                        <h4 class="footer-widget-title">Tautan Pendukung</h4>
                        <ul class="list-links">
                            <li>Kementerian Pendidikan dan Kebudayaan RI- <a href="#">http://www.kemdikbud.go.id/</a></li>
                            
                        </ul>
                    </div>
                </div>
                
                
            </div> <!-- /.row -->

            <div class="bottom-footer">
                <div class="row">
                    <div class="col-md-5">
                        <p class="small-text">&copy; Copyright 2014. Kementerian Pendidikan dan Kebudayaan. All Rights Reserved.</p>
                    </div> <!-- /.col-md-5 -->
                    
                </div> <!-- /.row -->
            </div> <!-- /.bottom-footer -->

        </div> <!-- /.container -->
    </footer> <!-- /.site-footer --> 
