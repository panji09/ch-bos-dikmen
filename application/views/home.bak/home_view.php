<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<?php include('inc/slider.php');?>
<!--==============================content================================-->
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
	
	<div class="sidebarkiri">
		
		
	<?php include('inc/menu_kanan_home.php');?>
	</div>
	<!-- sidebarkiri end -->
	
	<div class="maincontent">
	
		<!--------kiri----------------->
	<div id="content_tengah">
	
	 
      	<div class="page1-col-tengah">
            <h2 class="h2">Pengaduan Berdasarkan Kategori dan Status Penanganan</h2>
            <div id="chart_kategori" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
<?php
  /*
	foreach($report->result() as $row){
		$proses[]=$row->proses;
		$selesai[]=$row->selesai;
		$kat_pengaduan[]="'".$row->kategori."'";
	}
  */
?>
<?php 
/*
$(document).ready(function(){
	var proses = [<?=implode(",",$proses)?>];
	var selesai = [<?=implode(",",$selesai)?>];
	var ticks = [<?=implode(",",$kat_pengaduan)?>];
	 
	plot2 = $.jqplot('chart_kategori', [proses, selesai], {
		animate: !$.jqplot.use_excanvas,
		seriesDefaults: {
			renderer:$.jqplot.BarRenderer,
			pointLabels: { show: true }
		},
		axesDefaults: {
			tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
			tickOptions: {
			  angle: -30,
			  fontSize: '10pt'
			}
		},
		axes: {
			xaxis: {
				renderer: $.jqplot.CategoryAxisRenderer,
				ticks: ticks
			}
		},
		legend: {
			show: true,
			location: 'ne',
			placement: 'inside'
		},
		series:[
			{label:'Proses'},
			{label:'Selesai'}
	   ],
	   title:{
			text:'JUMLAH STATUS PENGADUAN BERDASARKAN KATEGORI <br>& STATUS PENANGANAN TAHUN 2011 s/d <?=date('Y')?>'
	   }
	});
 
	
});
*/
?>
$(document).ready(function(){
	var pending = [219,40,97,48];
	var proses = [219,40,97,48];
	var selesai = [1560,201,496,88];
	var ticks = ['Pertanyaan','Saran','Pengaduan'];
	 
	plot2 = $.jqplot('chart_kategori', [pending, proses, selesai], {
		animate: !$.jqplot.use_excanvas,
		seriesDefaults: {
			renderer:$.jqplot.BarRenderer,
			pointLabels: { show: true }
		},
		axes: {
			xaxis: {
				renderer: $.jqplot.CategoryAxisRenderer,
				ticks: ticks
			}
		},
		legend: {
			show: true,
			location: 'ne',
			placement: 'inside'
		},
		series:[
			{label:'Pending'},
			{label:'Proses'},
			{label:'Selesai'}
	   ],
	   title:{
			text:'JUMLAH STATUS PENGADUAN BERDASARKAN KATEGORI <br>& STATUS PENANGANAN 2014'
	   }
	});
 
	
});
</script>
			<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (pending / proses / selesai)</p>
			<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
			  
			  <tr>
				  <th>Kategori</th>
				  <th>Pending</th>
				  <th>Proses</th>
				  <th>Selesai</th>
				  <th>Total</th>
			  </tr>

			  <tr>
				  <td>Pertanyaan</td>
				  <td>219</td>
				  <td>219</td>
				  <td>1560</td>
				  <td>1779</td>
				  </tr>
			  <tr>
				  <td>Saran</td>
				  <td>40</td>
				  <td>40</td>
				  <td>201</td>
				  <td>241</td>
				  </tr>
			  <tr>
				  <td>Pengaduan</td>
				  <td>97</td>
				  <td>97</td>
				  <td>496</td>
				  <td>593</td>
				  </tr>
			  
			  <tr>
				  <td>Total</td>
				  <td>404</td>
				  <td>404</td>
				  <td>2345</td>
				  <td>2749</td>
			  </tr>
			
			
			</table>
			</p>
            
      </div>
	</div>
	<!--------endkiri----------------->
	
	</div>
	<!-- maincontent end -->
	
	<div class="sidebarkanan">
		
		<!-- BERITA -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/berita')?>">
						<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Berita
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				<ul>
					<?php foreach($berita->result() as $row):?>
					  <?php if($row->published):?>
						<li><a href="<?=site_url('home/berita/view/'.$row->id)?>"><?=$row->title?></a><br />
							<?php //$row->ringkasan?>
						</li>
					  <?php endif;?>
					<?php endforeach;?>
		        </ul>
		        <a href="<?=site_url('home/berita')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- BERITA END -->
		
		<!-- Pengumuman -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/pengumuman')?>">
						<img width="24" src="<?=$this->config->item('home_img')?>/1401263507_agt_announcements.png" /> Pengumuman
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				 <ul>
					  <?php foreach($pengumuman->result() as $row):?>
					    <?php if($row->published):?>
						<li><a href="<?=site_url('home/pengumuman/view/'.$row->id)?>"><?=$row->title?></a><br />
						  <?php //$row->ringkasan?>
						</li>
					    <?php endif;?>
					  <?php endforeach;?>
			  	
				</ul>
	              <a href="<?=site_url('home/pengumuman')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- Pengumuman END -->
		
		<!-- BERITA -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/peraturan')?>">
						<img width="24" src="<?=$this->config->item('home_img')?>/1401263626_administrative-docs.png" /> Peraturan Perundangan
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				<ul>
					<?php foreach($peraturan->result() as $row):?>
					  <?php if($row->published):?>
						<li><a href="<?=site_url('home/peraturan/view/'.$row->id)?>"><?=$row->title?></a><br />
							<?php //$row->ringkasan?>
						</li>
					  <?php endif;?>
					<?php endforeach;?>
		        </ul>
		        <a href="<?=site_url('home/peraturan')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- BERITA END -->
		
		<!-- BERITA -->
		<div class="boxkanan">
			<div class="menu_box_home">
					<ul>
						<li>
						<a href="<?=site_url('home/publikasi')?>">
						<img width="24" src="<?=$this->config->item('home_img')?>/1401263789_human-folder-public.png" /> Publikasi
						</a>
						
						</li>					
					</ul>
					
			</div>
			<div class="listbox">
				<ul>
					<?php foreach($publikasi->result() as $row):?>
					  <?php if($row->published):?>
						<li><a href="<?=site_url('home/publikasi/view/'.$row->id)?>"><?=$row->title?></a><br />
							<?php //$row->ringkasan?>
						</li>
					  <?php endif;?>
					<?php endforeach;?>
		        </ul>
		        <a href="<?=site_url('home/berita')?>" class="indexlink">Index</a>
			</div>
		</div>
		<!-- BERITA END -->
		
	
        
	
	</div>
	<!-- sidebarkanan end -->
      
      
      
      
      
      <div class="clear"></div>
    </div>
</section>
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>	
<script>
	Cufon.now();
</script>
</body>
</html>