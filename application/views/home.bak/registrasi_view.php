<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head.php"); ?>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <?php require_once('inc/logo.php') ?>
      <div class="clr"></div>
      <?php require_once("inc/menu.php"); ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize"> 
      <div class="clr"></div>
	  <div class="mainbar" style="width:inherit">
        <div class="article">
          <h2><img src="<?=$this->config->item('home_img')?>/<?=$this->uri->segment(1)=='faq' ? 'faq.png' : 'form.png'?>" width="48" style="border:0; padding:0;"/><a href="<?=$this->uri->segment(3)?>"><?=$title?></a></h2>
		  <style>
		  		#message-blue	{
				margin-bottom: 5px;
				}
			.blue-left	{
				background: url(<?=$this->config->item('admin_img')?>/table/message_blue.gif) top left no-repeat;
				color: #2e74b2;
				font-family: Tahoma;
				font-weight: bold;
				padding: 0 0 0 20px;
				}
			.blue-left a	{
				color: #2e74b2;
				font-family: Tahoma;
				font-weight: normal;
				text-decoration: underline;
				}
			.blue-right a	{
				cursor: pointer;
				}
			.blue-right	{
				width: 55px;}
		  </style>
		  <script>
			  $(document).ready(function() {
					$(".close-blue").click(function () {
						$("#message-blue").fadeOut("slow");
					});
					
				});
		  </script>
		  
          <div class="clr"></div>
<style>
#mainNav a {text-decoration:none}
/* = #mainNav2 & #mainNav3 & #mainNav4 & #mainNav5
----------------------------------------------------------------------------------------------------*/
#mainNav{clear:both}
#mainNav li{
	height:71px;
	list-style:none;
	float:left;
	background-color:#EBEBEB;
	background-image: url(<?=$this->config->item('home_img')?>/navBtn.gif);
	background-repeat: no-repeat;
	background-position: right top;
}

#mainNav li.current{
	background-color:#C36615;
	background-image: url(<?=$this->config->item('home_img')?>/navCurrentBtn.gif);
}

#mainNav li.lastDone{
	background-color:#7C8437;
	background-image: url(<?=$this->config->item('home_img')?>/navLastDoneBtn.gif);
}

#mainNav li.done{
	background-color:#7C8437;
	background-image: url(<?=$this->config->item('home_img')?>/navDoneBtn.gif);
}

#mainNav li a, #mainNav li a:link, #mainNav li a:visited, #mainNav li a:hover, #mainNav li a:active {
color:#ccc;
}

#mainNav li.lastDone a, #mainNav li.lastDone a:link, #mainNav li.lastDone a:visited, #mainNav li.lastDone a:hover, #mainNav li.lastDone a:active, #mainNav li.current a, #mainNav li.current a:link, #mainNav li.current a:visited, #mainNav li.current a:hover, #mainNav li.current a:active, #mainNav li.done a, #mainNav li.done a:link, #mainNav li.done a:visited, #mainNav li.done a:hover, #mainNav li.done a:active {
color:#fff;
}

#mainNav li.done a:hover, #mainNav li.lastDone a:hover  {
color:#FFFF99;
cursor:hand;
}

#mainNav li a em{
width:200px;
display:block;
margin:6px 0 0 10px;
font-style:normal;
font-weight:bold;
}

#mainNav li a span{
width:200px;
display:block;
margin-left:10px;
font-weight:normal;
}

#mainNav li.mainNavNoBg{
background-image:none;
}

#mainNav li a{
height:71px;
display:block;
}

/* #mainNav.fiveStep */
#mainNav.fiveStep li{width:182px;}
#mainNav.fiveStep li a{width:182px;}

/* #mainNav.fourStep */
#mainNav.fourStep li{width:227px;}
#mainNav.fourStep li.mainNavNoBg{width:229px;}
#mainNav.fourStep li a{width:227px;}

/* #mainNav.threeStep */
#mainNav.threeStep li{width:303px;}
#mainNav.threeStep li.mainNavNoBg{width:304px;}
#mainNav.threeStep li a{width:303px;}

/* #mainNav.twoStep */
#mainNav.twoStep li{width:455px;}
#mainNav.twoStep li a{width:455px;}


#wizardpanel {
	background-color: #F5F5F5; margin:5px;display:none;
}
#wizardcontent {
	height:300px
}
#wizardwrapper{width:920px;}



/* BUTTONS */
.buttons{margin:10px;clear:both}
.next {float:right}
.previous {float:left}
			</style>
			
			<div style="height:60px">
			<ul id="mainNav" class="threeStep">
			<?php if($this->uri->segment(2)=='step1'):?>
	  <li class="current"><a title="Tahap 1: Aktivasi Token"><em>Tahap 1: Aktivasi Token</em><span>Masukan kode token</span></a></li>
      <li><a title="Tahap 2: Registrasi Pengguna"><em>Tahap 2: Registrasi Pengguna</em><span>Isi data pengguna</span></a></li>
      <li class="mainNavNoBg"><a title="Tahap 3: Aktivasi Pengguna"><em>Tahap 3: Aktivasi Pengguna</em><span>Username dan Password sudah bisa digunakan</span></a></li>
			<?php elseif($this->uri->segment(2)=='step2'):?>
	  <li class="lastDone"><a title="Tahap 1: Aktivasi Token"><em>Tahap 1: Aktivasi Token</em><span>Masukan kode token</span></a></li>
      <li class="current"><a title="Tahap 2: Registrasi Pengguna"><em>Tahap 2: Registrasi Pengguna</em><span>Isi data pengguna</span></a></li>
      <li class="mainNavNoBg"><a title="Tahap 3: Aktivasi Pengguna"><em>Tahap 3: Aktivasi Pengguna</em><span>Username dan Password sudah bisa digunakan</span></a></li>
			<?php elseif($this->uri->segment(2)=='step3'):?>
	  <li class="done"><a title="Tahap 1: Aktivasi Token"><em>Tahap 1: Aktivasi Token</em><span>Masukan kode token</span></a></li>
      <li class="lastDone"><a title="Tahap 2: Registrasi Pengguna"><em>Tahap 2: Registrasi Pengguna</em><span>Isi data pengguna</span></a></li>
      <li class="current"><a title="Tahap 3: Aktivasi Pengguna"><em>Tahap 3: Aktivasi Pengguna</em><span>Username dan Password sudah bisa digunakan</span></a></li>
			<?php endif;?>
      
      
    </ul>
	</div>
          <p class="infopost"><!--Posted <span class="date">on 11 sep 2018</span> by <a href="#">Owner</a> &nbsp;|&nbsp; Filed under <a href="#">templates</a>, <a href="#">internet</a> with <a href="#" class="com">Comments <span>11</span></a>--></p>
			
			<form action="<?=site_url('registrasi/post_'.$this->uri->segment(2))?>" method="post" class="uniForm">
			<?php if($this->session->flashdata('valid')):?>
			<div id="okMsg">
			<?php if($this->uri->segment(2)=='step2'):?>
			<p>Silahkan cek kotak masuk/inbox email anda :) jika tidak ada kemungkinan berada  di kotak spam </p>
			<?php elseif($this->uri->segment(2)=='step3'):?>
			<p>Username & Password anda telah dikirim melalui email, trimakasih.</p>
			<?php endif;?>
     		</div>
			<?php endif;?>
			
			<?php if($this->uri->segment(2)=='step1'):?>
			<fieldset class="inlineLabels">
			<div class="ctrlHolder">
			
			  <label for="" id="kode">Kode</label>
			  <input type="text" name="kode" class="textInput medium required"/>
			  <p class="formHint">&nbsp;</p>
        	</div>	
			<div class="ctrlHolder">
				<label for="" id="namasek">CAPTCHA</label>
				<div align="right" style="width:511px">
				<?=recaptcha_get_html($publickey, $error);?>
				</div>
			  	
			  	
				
			</div>	
			</fieldset>
			<?=recaptcha_get_html($publickey, $this->session->flashdata('error_captcha'));?>
			<?php elseif($this->uri->segment(2)=='step2'):?>
			<fieldset class="inlineLabels">
			<h3>Selamat datang, <?=$t_user->username?><br /> Silahkan lengkapi identitas anda</h3>
			<div class="ctrlHolder">
			 <label for="">Nama</label>
			  	<input type="text" class="textInput medium required" name="nama"/>
			  <p class="formHint">&nbsp;</p>
			
		    </div>
			<div class="ctrlHolder">
			 <label for="">NIP</label>
			  	<input type="text" class="textInput medium required" name="nip"/>
			  <p class="formHint">&nbsp;</p>
			
		    </div>
			<div class="ctrlHolder">
			 <label for="">Jabatan di Dinas</label>
			  	<input type="text" class="textInput medium  required" name="dinas"/>
			  <p class="formHint">&nbsp;</p>
			
		    </div>
			<div class="ctrlHolder">
			 <label for="">Jabatan di Tim BOS</label>
			  	<input type="text" class="textInput medium required" name="tim_bos"/>
			  <p class="formHint">&nbsp;</p>
			
		    </div>
			
			<div class="ctrlHolder">
			  <label for="">Email</label>
			  	<input type="text" class="textInput medium validateEmail required" name="email"/>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
		
			  <label for="">Alamat Kantor</label>
			  	<textarea class="medium required" name="alamat_kantor" style="height:5em"></textarea>
			  
			
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			<label for="">Alamat Rumah</label>
			  	<textarea class="medium required" name="alamat_rumah" style="height:5em"></textarea>
			  
			
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  	<label for="">Telp Kantor</label>
			  	<input type="text" class="textInput small" name="telp_kantor"/>
			  	
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  	<label for="">Fax Kantor</label>
			  	<input type="text" class="textInput small" name="fax_kantor"/>
			  	
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  	<label for="">No. Handphone</label>
			  	<input type="text" class="textInput small" name="hp"/>
			  	
			  <p class="formHint">&nbsp;</p>
        	</div>
			</fieldset>
			<?php endif;?>
			
			<?php if($this->uri->segment(2)!='step3'):?>
			
			<div class="buttonHolder">
        	
        	<button class="primaryAction" type="submit">Kirim</button>
			<?php endif;?>
      </div>
</form>
<script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form.jquery.js"></script>
	 <script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form-validation.jquery.js"></script>
	 <script type="text/javascript">
       $(function(){
        $('form.uniForm').uniform({
			prevent_submit          : true
        });
      });
       </script>
			
          <p>&nbsp;</p>
        </div>
        <p class="pages"><!--<small>Page 1 of 2 &nbsp;&nbsp;&nbsp;</small> <span>1</span> <a href="#">2</a> <a href="#">&raquo;</a>--></p>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
</div>
</div>
</body>
</html>
