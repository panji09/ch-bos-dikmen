<section id="header-content">
  <div class="main">
    <div class="slider">
      <ul class="items">
         <li><img src="<?=$this->config->item('home_img')?>/slider_1.jpg" alt="">
         	<div class="banner">
            	<p><strong class="font-2">BOP</strong><br /><strong class="font-1">ketahui - kawal - miliki</strong></p>
                <!-- <a href="<?=site_url('home/about')?>">Read More</a> -->
         	</div>
        </li>
         <li><img src="<?=$this->config->item('home_img')?>/slider_2.jpg" alt="">
         	<div class="banner">
            	<p><strong class="font-1">Ketahui programnya, <br /> Kawal penggunaannya,<br /> Miliki keberhasilannya</strong></p>
                <!-- <a href="<?=site_url('home/about')?>">Read More</a> -->
         	</div>
        </li>
         <li><img src="<?=$this->config->item('home_img')?>/slider_3.jpg" alt="">
         	<div class="banner">
            	<p><strong class="font-1">Yang miskin dapat kartu <br />karena yang kaya sudah mampu</strong></p>
                <!-- <a href="<?=site_url('home/about')?>">Read More</a> -->
         	</div>
        </li>
         <li><img src="<?=$this->config->item('home_img')?>/slider_4.jpg" alt="">
         	<div class="banner">
            	<p><strong class="font-1">Dengan KJP, tidak ada lagi <br />siswa yang putus sekolah</strong></p>
                <!-- <a href="<?=site_url('home/about')?>">Read More</a> -->
         	</div>
         </li>
         <li><img src="<?=$this->config->item('home_img')?>/slider_5.jpg" alt="">
         	<div class="banner">
            	<p><strong class="font-1">Mari sama-sama kita pantau pelaksanannya<br />untuk pendidikan yang lebih baik</strong></p>
                <!-- <a href="<?=site_url('home/about')?>">Read More</a> -->
         	</div>
         </li>
      </ul>
      <div class="page_slide">
          <ul>
            <li><a href="#"><img src="<?=$this->config->item('home_img')?>/slider-1-small.jpg" alt=""></a></li>
            <li><a href="#"><img src="<?=$this->config->item('home_img')?>/slider-2-small.jpg" alt=""></a></li>
            <li><a href="#"><img src="<?=$this->config->item('home_img')?>/slider-3-small.jpg" alt=""></a></li>
            <li><a href="#"><img src="<?=$this->config->item('home_img')?>/slider-4-small.jpg" alt=""></a></li>
            <li><a href="#"><img src="<?=$this->config->item('home_img')?>/slider-5-small.jpg" alt=""></a></li>
          </ul>
      </div>  
    </div>
  </div>
</section>