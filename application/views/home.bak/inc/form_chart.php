
<div class="gadget">
          <ul class="ex_menu" style="font-size:14px;">
		  	<li><span><a href="#">Tentang BSM </a></span></li>
			
          </ul>
		  <div class="clr"></div>
          
        </div>
<div class="gadget">
		<form id="form" method="post" action="<?=site_url('home/post_graph')?>">
          <h2 class="star"><span><img src="<?=$this->config->item('home_img')?>/presentation.png">Statistik Pengaduan Berdasarkan: </span></h2>
          <div class="clr"></div>
		  <p>
		    <?php
		  	$kategori=array(
				'r1' => 'Kategori',
				'r2' => 'Sumber Informasi',
				'r3' => 'Jumlah & Status',
				'r4' => 'Kategori & Status',
				'r5' => 'Pelaku & Status',
				'r6' => 'Status Provinsi',
				'r7' => 'Media'
			);
		  ?>
		    <select name="chart">
	          <?php foreach($kategori as $key => $value):?>
		      <?php if($this->uri->segment(3)==$key):?>
		      <option value="<?=$key?>" selected="selected">
	          <?=$value?>
	          </option>
	          <?php else:?>
		      <option value="<?=$key?>" >
	          <?=$value?>
	          </option>
	          <?php endif;?>
              <?php endforeach;?>	
	        </select>
	      
		  
		  <label>Provinsi </label>
		 
          <select name="prov" id="provinsi">
            <option value="all">-Semua-</option>
            <?php foreach($prov->result() as $row):?>
            <?php if($this->uri->segment(4)==$row->KdProv):?>
            <option value="<?=$row->KdProv?>" selected="selected">
              <?=$row->NmProv?>
            </option>
            <?php else:?>
            <option value="<?=$row->KdProv?>">
              <?=$row->NmProv?>
            </option>
            <?php endif;?>
            <?php endforeach;?>
          </select>
          
          <label>Kab / Kota</label>
          
          <select id="kabkota" name="kabkota" >
            <option value="all">-Semua-</option>
            <?php if($this->uri->segment(2)=='graph' && $this->uri->segment(3)!='all'):?>
            <?php foreach($kabkota->result() as $row):?>
            <?php if($this->uri->segment(5)==$row->KdKab):?>
            <option value="<?=$row->KdKab?>" selected="selected" >
              <?=$row->NmKab?>
            </option>
            <?php else:?>
            <option value="<?=$row->KdKab?>">
              <?=$row->NmKab?>
            </option>
            <?php endif;?>
            <?php endforeach;?>
            <?php endif;?>
          </select>
		  
          <label>Tahun</label>
          
          <select id="tahun" name="tahun" >
            <option value="all">-Semua-</option>
            <?php for($i=2011;$i<=date('Y');$i++):?>
			<?php if($this->uri->segment(2)=='graph' && $this->uri->segment(6)==$i):?>
				<option value="<?=$i?>" selected="selected"><?=$i?></option>
			<?php else:?>
				<option value="<?=$i?>"><?=$i?></option>
			<?php endif;?>
			<?php endfor;?>
            </select>
		  
		  </p>

          <input name="submit" type="submit" class="button_submit" value="Proses" />
<br />
<div class="clr"></div>
		  <label></label>
		</form>
        </div>
<div class="gadget" style="background-color:#F8F8F8;" >
<h2 class="star"></h2>
          <div class="clr"></div>
<style>
form_login {
margin: 0px;
padding: 15px 25px 25px 20px;
border: 1px solid #F0F0F0;
background: #F8F8F8;
border-image: initial;
}
#form_login p {
	margin-left : 5px;
}
#form_login input[type="text"], input[type="password"]{
	width:140px
}
#form_login p{
	margin:0px
	padding:0px
}
</style>
<form id="form_login" method="post" action="<?=base_url()?>aplikasi/loginPost.php" name="loginform">		
				<p>	
				  <label for="name"><strong>Username</strong></label>
				  
					<input type="text" tabindex="1" value="" title="Your Username" name="txtuserid" id="username" class="example">
				</p>
			
				<p>
					<label for="password"><strong>Password</strong></label>
					
					<input type="password" tabindex="2" value="" title="Your Password" name="txtpassword" id="password" class="example">
				</p>
                <p>
				 <a href="<?=site_url('registrasi')?>" >registrasi</a> | <a href="#" id="forget">lupa password</a>
				</p>
				<div id="dialog-registrasi" title="Registrasi" style='display:none'>
					<p>Untuk regristrasi ke <b>Aplikasi Layanan Masyarakat & Penanganan Pengaduan BOS</b>, <br> pemohon Tim BOS Provinsi dan Kab/Kota dapat mengirimkan permintaan username dan password kepada Manajemen BOS Pusat di bos@kemdikbud.go.id.  Jawaban yang berisi username dan password akan disampaikan ke alamat pemohon</p>
				</div>
				<div id="dialog-forget" title="Lupa Password" style='display:none'>
					<p>Pemohon dapat melakukan permintaan penggantian username dan password melalui bos@kemdikbud.go.id</p>
				</div>

				<p class="no-border">
					<input type="submit" tabindex="3" value="Submit" class="button_submit" name="btnlogin" >	
				</p>
					
  </form>
</div>