<?php 
// kategori & status
if($report=true):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<?php if($daerah=='all'): $title='NASIONAL';?>
	<div id="chart_daerah" align="center" style="height:1300px"><!--Chart Di Load disini--></div>
<?php elseif($daerah=='provinsi'): $title='PROVINSI';?>
	<div id="chart_daerah" align="center" style="height:1000px"><!--Chart Di Load disini--></div>
<?php elseif($daerah=='kabkota'): $title='KABUPATEN / KOTA';?>
	<div id="chart_daerah" align="center" style="height:600px"><!--Chart Di Load disini--></div>
<?php endif;?>


<script>
$(document).ready(function(){
	var proses = [0,2,3,2,1,3];
	var selesai = [2,24,10,34,34,11];
	var ticks = ['Kab. Kepulauan Seribu','Kota Jakarta Barat','Kota Jakarta Pusat','Kota Jakarta Selatan','Kota Jakarta Timur','Kota Jakarta Utara'];
	 
	plot2 = $.jqplot('chart_daerah', [proses, selesai], {
		animate: !$.jqplot.use_excanvas,
		seriesDefaults: {
			renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
                shadowAngle: 135,
			rendererOptions: {
                    barDirection: 'horizontal'
                }
		},
		axes: {
			
			yaxis: {
				renderer: $.jqplot.CategoryAxisRenderer,
				ticks: ticks,
				
			},
			xaxis: {
				min:0,
				tickInterval: 10,
				tickOptions: { formatString:'%d' }
			},
		},
		legend: {
			show: true,
			location: 'ne',
			placement: 'inside'
		},
		series:[
			{label:'Proses'},
			{label:'Selesai'}
	   ],
	   title:{
			text:'JUMLAH STATUS PENANGANAN PENGADUAN PER PROVINSI <br> TAHUN 2011 s/d 2014'
	   }
	});
 
	
});
</script>

<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>

<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	
			<td><strong>Kabupaten / Kota</strong></td>
		<td><strong>Proses</strong></td>
	<td><strong>Selesai</strong></td>
	<td><strong>Total</strong></td>
</tr>

<tr>
			<td>Kab. Kepulauan Seribu</td>
		
	<td>0</td>
	<td>2</td>
	<td>2</td>
</tr>
<tr>
			<td>Kota Jakarta Barat</td>
		
	<td>2</td>
	<td>24</td>
	<td>26</td>
</tr>
<tr>
			<td>Kota Jakarta Pusat</td>
		
	<td>3</td>
	<td>10</td>
	<td>13</td>
</tr>
<tr>
			<td>Kota Jakarta Selatan</td>
		
	<td>2</td>
	<td>34</td>
	<td>36</td>
</tr>
<tr>
			<td>Kota Jakarta Timur</td>
		
	<td>1</td>
	<td>34</td>
	<td>35</td>
</tr>
<tr>
			<td>Kota Jakarta Utara</td>
		
	<td>3</td>
	<td>11</td>
	<td>14</td>
</tr>

</table>
<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>