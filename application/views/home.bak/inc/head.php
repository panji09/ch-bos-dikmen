<head>
    <!--<title><?=$title?></title>-->
    <title>layanan Masyarakat</title>
    <meta charset="utf-8">
	<link href="<?=$this->config->item("home_plugin")?>/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="<?=$this->config->item("home_plugin")?>/jqplot/jquery.jqplot.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$this->config->item('home_css')?>/reset.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$this->config->item('home_css')?>/style.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$this->config->item('home_css')?>/grid_12.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$this->config->item('home_css')?>/slider.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=$this->config->item('home_css')?>/tabs.css">
	<link rel="stylesheet" type="text/css" media="screen" href="<?=$this->config->item('home_css')?>/breadcrumb.css">
    <script src="<?=$this->config->item('home_js')?>/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item('home_plugin')?>/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=$this->config->item('home_js')?>/jquery.easing.1.3.js"></script>
    
	<script src="<?=$this->config->item('home_js')?>/cufon-yui.js"></script>
    <script src="<?=$this->config->item('home_js')?>/Vegur-L_300.font.js"></script>
    <script src="<?=$this->config->item('home_js')?>/Vegur-M_500.font.js"></script>
    <script src="<?=$this->config->item('home_js')?>/Vegur-R_400.font.js"></script>
    <script src="<?=$this->config->item('home_js')?>/cufon-replace.js"></script>
    <script src="<?=$this->config->item('home_js')?>/tabs.js"></script>
    <script src="<?=$this->config->item('home_js')?>/FF-cash.js"></script>
    
	<!-- slider -->
	<?php if($this->uri->segment(1)==''):?>
	<script src="<?=$this->config->item('home_js')?>/tms-0.3.js"></script>
	<script src="<?=$this->config->item('home_js')?>/tms_presets.js"></script>
    
	<script>
		$(window).load(function(){
			$('.slider')._TMS({
			prevBu:'.prev',
			nextBu:'.next',
			pauseOnHover:true,
			pagNums:false,
			duration:800,
			easing:'easeOutQuad',
			preset:'Fade',
			slideshow:7000,
			pagination:'.page_slide',
			waitBannerAnimation:false,
			banners:'fromLeft'
			})
		}) 	
    </script>
	<?php endif;?>
	<!-- end slider -->
	
	<!--jqplot-->
	
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/jquery.jqplot.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.barRenderer.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.pointLabels.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.highlighter.min.js"></script>
	
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
	<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>

	<!--end jqplot-->
	<!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
    	<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
	<![endif]-->
	
	<!-- Label Form -->
    <!--
    <link rel="stylesheet" href="<?=$this->config->item('home_plugin')?>/label_form/css/layout.css" type="text/css" media="all" charset="utf-8" />    
    
    <script src="<?=$this->config->item('home_plugin')?>/label_form/js/jquery.infieldlabel.min.js" type="text/javascript" charset="utf-8"></script>
    -->
    <script type="text/javascript" charset="utf-8">
        //$(function(){ $("label").inFieldLabels(); });
    </script>
    <!--[if lte IE 6]>
        <style type="text/css" media="screen">
            form label {
                    background: #fff;
            }
        </style>
    <![endif]-->
<!-- Label Form -->

<!-- gallery -->
<?php if($this->uri->segment(2)=='gallery'):?>
<link rel="stylesheet" href="<?=$this->config->item("home_plugin")?>/galleriffic/css/galleriffic-3.css" type="text/css" />
<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/galleriffic/js/jquery.history.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/galleriffic/js/jquery.galleriffic.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/galleriffic/js/jquery.opacityrollover.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	// We only want these styles applied when javascript is enabled
	$('div.navigation').css({'width' : '300px', 'float' : 'left'});
	$('div.content').css('display', 'block');

	// Initially set opacity on thumbs and add
	// additional styling for hover effect on thumbs
	var onMouseOutOpacity = 0.67;
	$('#thumbs ul.thumbs li').opacityrollover({
		mouseOutOpacity:   onMouseOutOpacity,
		mouseOverOpacity:  1.0,
		fadeSpeed:         'fast',
		exemptionSelector: '.selected'
	});
	
	// Initialize Advanced Galleriffic Gallery
	var gallery = $('#thumbs').galleriffic({
		delay:                     2500,
		numThumbs:                 15,
		preloadAhead:              10,
		enableTopPager:            true,
		enableBottomPager:         true,
		maxPagesToShow:            7,
		imageContainerSel:         '#slideshow',
		controlsContainerSel:      '#controls',
		captionContainerSel:       '#caption',
		loadingContainerSel:       '#loading',
		renderSSControls:          true,
		renderNavControls:         true,
		playLinkText:              'Play Slideshow',
		pauseLinkText:             'Pause Slideshow',
		prevLinkText:              '&lsaquo; Previous Photo',
		nextLinkText:              'Next Photo &rsaquo;',
		nextPageLinkText:          'Next &rsaquo;',
		prevPageLinkText:          '&lsaquo; Prev',
		enableHistory:             true,
		autoStart:                 false,
		syncTransitions:           true,
		defaultTransitionDuration: 900,
		onSlideChange:             function(prevIndex, nextIndex) {
			// 'this' refers to the gallery, which is an extension of $('#thumbs')
			this.find('ul.thumbs').children()
				.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
				.eq(nextIndex).fadeTo('fast', 1.0);
		},
		onPageTransitionOut:       function(callback) {
			this.fadeTo('fast', 0.0, callback);
		},
		onPageTransitionIn:        function() {
			this.fadeTo('fast', 1.0);
		}
	});

	/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

	// PageLoad function
	// This function is called when:
	// 1. after calling $.historyInit();
	// 2. after calling $.historyLoad();
	// 3. after pushing "Go Back" button of a browser
	function pageload(hash) {
		// alert("pageload: " + hash);
		// hash doesn't contain the first # character.
		if(hash) {
			$.galleriffic.gotoImage(hash);
		} else {
			gallery.gotoIndex(0);
		}
	}

	// Initialize history plugin.
	// The callback is called at once by present location.hash. 
	$.historyInit(pageload, "advanced.html");

	// set onlick event for buttons using the jQuery 1.3 live method
	$("a[rel='history']").live('click', function(e) {
		if (e.button != 0) return true;
		
		var hash = this.href;
		hash = hash.replace(/^.*#/, '');

		// moves to a new page. 
		// pageload is called at once. 
		// hash don't contain "#", "?"
		$.historyLoad(hash);

		return false;
	});

	/****************************************************************************************/
});
</script>
<?php endif;?>
<!-- end gallery -->

<!-- js prov, kabkota, dan pengaduan -->
<?php if($this->uri->segment(1)=="home" || $this->uri->segment(1)=="registrasi" || $this->uri->segment(2)==""):?>
	
<script type="text/javascript">
	$(document).ready(function() {
		
		$("#kabkota").change(function() {
			$("#kecamatan").empty();
			$("#kecamatan").append('<option value="">-Pilih-</option>');
			$("#desa").empty();
			$("#desa").append('<option value="">-Pilih-</option>');
			$.post('<?=site_url('services/get_kecamatan')?>',
			{ kabkota_id : $("#kabkota option:selected").val() },
			function(data) {
				$.each(data, function(i, item){
					$("#kecamatan").append(
						'<option value="' + item.id + '">'+item.name + '</option>'
					);
				})
			},
			'json'
			);
		});
		
		
		$("#jenjang").change(function() {
			$("#namasek").empty();
			if($("#jenjang").val()==4){
				$("#namasek").append('lokasi');
			}else{
				$("#namasek").append('Nama Sekolah');
			}
		});
		
		/*function showketRP(){
			if($('input[name=KdKategori]:checked').val()==4){
				$("#KetRpKat4").show();
			}else{
				$("#KetRpKat4").hide();
			}
		}
		showketRP();
		$('input[name=KdKategori]').click(function() {
			showketRP();
		});*/
		$("#KdKategori").change(function() {
			if($("#KdKategori").val()==7){
				$("#KetRpKat4_text").addClass("required");
				$("#KetRpKat4").show();
			}else{
				$("#KetRpKat4_text").attr('class', 'textInput auto validateInteger');
				$("#KetRpKat4").attr('class', 'ctrlHolder');
				$("#KetRpKat4").hide();
			}
		});
		function showlainSumber(){
			if($('input[name=KdSumber]:checked').val()==8){
				$("#LainSumber").show();
			}else{
				$("#LainSumber").hide();
			}
		}
		showlainSumber();
		$('input[name=KdSumber]').click(function() {
			showlainSumber();
		});
		
				
		function showpLain(){
			if($('input[name=pLain]:checked').val()){
				$("#KetpLain").show();
			}else{
				$("#KetpLain").hide();
			}
		}
		showpLain();
		$('input[name=pLain]').click(function() {
			showpLain();
		});
		$("a[href$='.pdf']").each(function(){
			$(this).attr('href', '<?=site_url('download/view')?>/' + encodeHex($(this).attr('href')));
		});
		$("a[href$='.pptx']").each(function(){
			$(this).attr('href', '<?=site_url('download/view')?>/' + encodeHex($(this).attr('href')));
		});
		$("a[href$='.ppt']").each(function(){
			$(this).attr('href', '<?=site_url('download/view')?>/' + encodeHex($(this).attr('href')));
		});
		$("a[href$='.docx']").each(function(){
			$(this).attr('href', '<?=site_url('download/view')?>/' + encodeHex($(this).attr('href')));
		});
		$("a[href$='.doc']").each(function(){
			$(this).attr('href', '<?=site_url('download/view')?>/' + encodeHex($(this).attr('href')));
		});
		$("a[href$='.xlsx']").each(function(){
			$(this).attr('href', '<?=site_url('download/view')?>/' + encodeHex($(this).attr('href')));
		});
		$("a[href$='.xls']").each(function(){
			$(this).attr('href', '<?=site_url('download/view')?>/' + encodeHex($(this).attr('href')));
		});
	});
	
</script>

	<style>
	.side-a {
		float: left;
		width: 30%;
	}
	
	.side-c {
		float: left;
		width: 30%;
	}
	
	.side-b { 
		float: left;
		width: 30%;
	}
	</style>
	<link href="<?=$this->config->item('home_css')?>/uni-form.css" media="all" rel="stylesheet"/>
	<link href="<?=$this->config->item('home_css')?>/default.uni-form.css" media="all" rel="stylesheet"/>
	
	<?php endif;?>
<!-- js prov, kabkota, dan pengaduan -->

<!-- faq -->
<?php if($this->uri->segment(2)=='faq' || $this->uri->segment(2)=='pengaduan'):?>
<style>
#message-blue	{
margin-bottom: 5px;
}
.blue-left	{
background: url(<?=$this->config->item('admin_img')?>/table/message_blue.gif) top left no-repeat;
color: #2e74b2;
font-family: Tahoma;
font-weight: bold;
padding: 0 0 0 20px;
}
.blue-left a	{
color: #2e74b2;
font-family: Tahoma;
font-weight: normal;
text-decoration: underline;
}
.blue-right a	{
cursor: pointer;
}
.blue-right	{
width: 55px;}
</style>
<script>
$(document).ready(function() {
	$(".close-blue").click(function () {
		$("#message-blue").fadeOut("slow");
	});
	
});
</script>
<style type="text/css">
.faqsection {
	margin: 10px 0;
}

div.faq {
	margin: 5px 0 0 10px;
}

div.faq .question {
	color: #2763A5;
	cursor:  pointer;
	padding-left: 10px;
	background: url(raquo.gif) no-repeat left 5px;
}

div.faq .question.active {
	background-image: url(raquo-down.gif);
}

div.faq .answer  {
	margin-left: 20px;	
}

div.faq ul,
div.faq ol {
	margin: 0 0 10px 20px;
}
.question p, .answer p{
	margin:0;
	padding:0;
}
.article h2{
	margin: 4px 0;
	padding: 4px 0;
}
</style>
<script>

SSS_faq = {
init : function() {
$('div.faq .answer').not(':first').slideToggle('fast');
$('div.faq .question').click(function() { SSS_faq.toggle(this) });
},
toggle : function(elt) {
$(elt).toggleClass('active');
$(elt).siblings('.answer').slideToggle('fast');
}
}
$(function() {
SSS_faq.init();
});

</script>
<?php endif;?>
<!-- end faq -->

<!--hightlight-->
<style type="text/css">
.highlight {
    background-color: #fff34d;
    -moz-border-radius: 5px; /* FF1+ */
    -webkit-border-radius: 5px; /* Saf3-4 */
    border-radius: 5px; /* Opera 10.5, IE 9, Saf5, Chrome */
    -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* FF3.5+ */
    -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Saf3.0+, Chrome */
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Opera 10.5+, IE 9.0 */
}

.highlight {
    padding:1px 4px;
    margin:0 -4px;
}
</style>
<script type="text/javascript" src="<?=$this->config->item("home_js")?>/highlight.js"></script>
<!--end hightlight-->

<!--share this-->
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "7163e598-47a3-41cd-b266-f25720c8a725", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<!--./share this-->
</head>