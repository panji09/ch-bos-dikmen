<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1">
			
			<div class="judul_page">
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="<?=site_url('home/faq')?>" class="current">Frequently Asked Questions (FAQ)</a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Frequently Asked Questions (FAQ)
			</div>
	
		<div style="clear: both;"></div>
		<div id="message-blue">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blue-left">Klik pertanyaan untuk menampilkan jawaban</td>
					<td class="blue-right"><a class="close-blue"><img src="<?=$this->config->item('admin_img')?>/table/icon_close_blue.gif"   alt="" style="border:0; margin:0;padding:0" /></a></td>
				</tr>
				</table>
		  	</div>
			
				<?php foreach($faq_kat->result() as $row):?>
					<h2><span style="color:#000000;" class="western"><?=$row->faq_kategori?></span></h2>
					<?php 
					$faq=$this->Select_db->faq('by_kategori',array('id_kategori' => $row->id));
					foreach($faq->result() as $row_faq):
					?>
					<div class="faq">
						<div class="question"><?=$row_faq->tanya?></div>
						<div class="answer"><?=$row_faq->jawab?></div>
					</div>
					<?php endforeach;?>
				<?php endforeach;?>
		<?php include('inc/share_button.php');?>
	
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
      <?php include('inc/menu_kanan.php');?>
	  <!-- sidebar end -->
      <div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
</script>
</body>
</html>