<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head.php"); ?>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <?php require_once('inc/logo.php') ?>
      <div class="clr"></div>
      <?php require_once("inc/menu.php"); ?>
    </div>
  </div>
  <div class="content">
    <div class="content_resize"> 
      <div class="clr"></div>
	  <div class="mainbar" style="width:inherit">
	  <div class="article">
          <h2><img src="<?=$this->config->item('home_img')?>/form.png" width="48" style="border:0; padding:0;"/><a href="<?=$this->uri->segment(3)?>">Pengaduan SMS 1771 </a></h2  
          >
          <div class="clr"></div>
      </div>
	  <style>
	  .postmeta {
		background: none repeat scroll 0 0 #F8F8F8;
		border: 1px solid #EBEBEB;
		margin: 20px 15px 15px 10px;
		padding: 5px;
		}
		
	  #nav-filter{
	  	margin:0 0 12px;
		padding:0 24px 0 24px;
	  }
	  #nav-filter input[type="submit"]{
	  	margin-bottom:10px;
		
	  }
	  #nav-filter ul{
	  	width:100%;
    	
	  }
	  #nav-filter li{
	  	width:20%;
    	
	  }
	  #nav-filter .buttonHolder{
	  	padding: 0.5em;
		margin: 0.5em 0 0;
    	
	  }
	  #nav-filter .ctrlHolder{
	  	padding: 0;
		
    	
	  }
	  </style>
	  
	  <div id="nav-filter">
	  
	  
	  <form method="post" action="<?=site_url('home/post_filter/lihat_pengaduan_sms')?>" class="uniForm">
	  	 <fieldset class="inlineLabels">
	  	
	  	<div class="ctrlHolder">
		
			<ul class="alternate">
				<li>
				Tahun 
				<select name="tahun">
						<option value="">-Semua-</option>
					<?php for($i=2012; $i<=date("Y"); $i++):?>
						<option value="<?=$i?>" <?=($i==$this->session->userdata('tahun')? 'selected="selected"' : '')?>><?=$i?></option>
					<?php endfor;?>
				</select>
				</li>
				<li>
				Triwulan 
				<select name="triwulan">
					<option value="">-Semua-</option>
					<?php for($i=1; $i<=4; $i++):?>
						<option value="<?=$i?>" <?=($i==$this->session->userdata('triwulan')? 'selected="selected"' : '')?>><?=$i?></option>
					<?php endfor;?>
				</select> 
				</li>
				<li>
				Provinsi 
				<select name="prov" id="provinsi">
					<option value="">-Semua-</option>
					<?php foreach($prov->result() as $row):?>
					
					<option value="<?=$row->KdProv?>" <?=($row->KdProv==$this->session->userdata('provinsi')? 'selected="selected"' : '')?>> 
					<?=$row->NmProv?> 
					</option>
					
					<?php endforeach;?>
				  </select>
				</li>
				<li>
				Kabkota 
				<select id="kabkota" name="kabkota" >
				<option value="">-Semua-</option>
				<?php foreach($kabkota->result() as $row):?>
					<option value="<?=$row->KdKab?>" <?=($row->KdKab==$this->session->userdata('kabkota')? 'selected="selected"' : '')?>>
					  <?=$row->NmKab?>
					</option>
				<?php endforeach;?>
				
				</select> 
				</li>
			</ul>
		</div>
		 </fieldset>
		
		
		
		<div class="buttonHolder">
        	
        	<button class="primaryAction" type="submit">Filter</button>
      	</div>
		</form>
	  </div>
		
	  </style>
	  <?php foreach($pengaduan->result() as $row):?>        
	  <div class="article">

          <div class="clr"></div>
          <p class="infopost" style="color:#000000">Di Posting <?=mysqldatetime_to_date_id($row->tanggal)?> Oleh <?=substr_replace($row->msisdn, 'xxx', -3, 3)?>, <?=$row->alamat?><br /><?=$row->nama_sekolah?>, <?=$row->kabkota?>, <?=$row->prov?></p>
			<p style="color:#000000"><?=$row->deskripsi?></p>
		  
		  
      </div>
	  <?php
	  	//echo $row->kdpengaduan;
	  	$reply=$this->Select_db->reply_pengaduan($row->kdpengaduan);
		if($reply->num_rows()!=0):
	  ?>
	  <div class="article">
          
          <div class="clr"></div>
		  <?php foreach($reply->result() as $isi):?>
          <div class="comment" style="background: none repeat scroll 0 0 #F8F8F8;border-color: #F0F0F0;
    border-style: solid;
    border-width: 1px 1px 0;
    margin: 12px 10px;
    padding: 0;"> <a href="#"><img width="65" height="40" class="userpic" alt="" src="<?=$this->config->item('home_img')?>/logo_bos_mini.png"></a>
            <p style="color:#000000"><a href="#" >Tim BOS</a> Menjawab:<br>
              <?=mysqldatetime_to_date_id($isi->TglUpdate)?></p>
            <p style="color:#000000"><?=$isi->Uraian?></p>
          </div>
		  <?php endforeach;?>
        </div>
		<?php endif;?>
		<p class="postmeta"></p>
	  <?php endforeach;?>
        </div>
		&nbsp;
        <?=$this->pagination->create_links();?>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <?php require_once("inc/footer.php") ?>
  </div>
</div>
</body>
</html>
