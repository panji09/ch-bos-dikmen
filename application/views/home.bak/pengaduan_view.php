<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<style>
h4{
    font-size: 17.5px !important ; 
}
.page1-col1, .judul_page, .container_12 .grid_8  {
    width: 1000px;
}
</style>
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1">
			
			<div class="judul_page">
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="<?=site_url('home/pengaduan')?>" class="current">Pengaduan Online</a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Pengaduan Online
			</div>
			<div style="clear: both;"></div>
		<div id="message-blue">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blue-left">Mengadu secara komplit dan bertanggung jawab
sebelum anda bertanya lebih jauh, silahkan <a href="<?=site_url('home/faq');?>">lihat FAQ</a></td>
					<td class="blue-right"><a class="close-blue"><img src="<?=$this->config->item('admin_img')?>/table/icon_close_blue.gif"   alt="" style="border:0; margin:0;padding:0" /></a></td>
				</tr>
				</table>
		  </div>
			<form action="<?=site_url('home/post_pengaduan')?>" method="post" class="uniForm">
			<?php if($this->session->flashdata('valid')=='true'):?>
			<div id="okMsg">
			<p>
			  Terima kasih, telah berkontribusi untuk meningkatkan kualitas pendidikan yang lebih baik</p>
     		</div>
			<?php endif;?>
		<fieldset class="inlineLabels">
			<h4>Lokasi Kejadian</h4>
			<div class="ctrlHolder">
			  <label for="">Daerah</label><ul class="alternate">
			  	
				<li>
					
					Kabupaten / Kota
					<select id="kabkota" name="kabkota" class="selectInput required" >
                      <option value="">-Pilih-</option>
		      <?php
			$kabkota=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));		
		      ?>
		      <?php foreach($kabkota->result() as $row):?>
		      <option value="<?=$row->id?>"><?=$row->name?></option>
		      <?php endforeach;?>
                    </select>
					
				</li>
				<li>
					
					Kecamatan
					<select id="kecamatan" name="kecamatan" class="selectInput required">
                      <option value="">-Pilih-</option>
                    </select>
					
				</li>
			  </ul>
			  
			  <p class="formHint">&nbsp;</p>
        	</div>
		<div class="ctrlHolder">
			  <label for="">Program</label>
			  <select name="program" id="program" class="medium required">
						
						<option value="">-Pilih-</option>
						<?php
						    $program=$this->Select_db->program()->result();
						    foreach($program as $row):
						?>
						<option value="<?=$row->id?>"><?=$row->name?></option>
						<?php endforeach;?>
			  </select>
			  <p class="formHint">&nbsp;</p>
        	</div>	
		<div class="ctrlHolder">
			  <label for="">Jenjang</label>
			  <select name="jenjang" id="jenjang" class="medium required">
						<option value="">-Pilih-</option>
						<?php 
						  $jenjang = $this->Select_db->jenjang()->result();
						  foreach($jenjang as $row):?>
						  <option value="<?=$row->id?>"><?=$row->name?></option>
						<?php endforeach;?>
			  </select>
			  <p class="formHint">&nbsp;</p>
        	</div>
			
			<div class="ctrlHolder">
			
			  <label for="" id="namasek">Nama Sekolah </label>
			  <input type="text" name="sekolah" class="textInput medium required"/>
			  <p class="formHint">&nbsp;</p>
        	</div>	
			</fieldset>
			<fieldset class="inlineLabels">
			<h4>Kategori Pengaduan</h4>
			<div class="ctrlHolder">
			  
				<label for="">Kategori</label>
				<select name="kategori" id="kategori" class="medium required KdKategori">
				  <option value="">-Pilih-</option>
				  <?php 
				    $kategori = $this->Select_db->kategori()->result();
				    foreach($kategori as $row):?>
				    <option value="<?=$row->id?>"><?=$row->name?></option>
				  <?php endforeach;?>
                  
                  
                </select>
				<p class="formHint">&nbsp;</p>
			  <p class="formHint">&nbsp;</p>
        	</div>	
			
		</fieldset>
		<fieldset class="inlineLabels">
			<h4>Identitas Pelapor </h4>
			<div class="ctrlHolder" >
			  <label for="">Profesi</label>
			    <ul>
				<li>
				    <label>
				    <select class="medium required" name="sumber_info" id='KdSumber'>
					<option value="">-Pilih-</option>
					<?php 
					  $sumber = $this->Select_db->sumber_info($role='home')->result();
					  foreach($sumber as $row):?>
					  <option value="<?=$row->id?>"><?=$row->name?></option>
					<?php endforeach;?>
				    </select>
				    </label><br>
				</li>
				<li>
				    <label>
				    <input style='display: none;' id='sumber_lain' type="text" class="textInput medium" name="sumber_lain"/>
				    </label>
				</li>
			    </ul>
			  	
				
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  <label>Nama</label>
			  <ul>
			  
			  <li>
			  	<label><p style="display:none">Nama</p>
			  	<input type="text" class="textInput medium required" name="nama"/><br />
			  
			  	</label>
			  </li>
			  <li>
			  <label>Apakah anda ingin nama anda ditampilkan? <input type="checkbox" name="param[tampil_nama]" value="1" />
			  </label>
			  </li>
			  </ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  <label for="">Email</label>
			  	<input type="text" class="textInput medium validateEmail required" name="email"/>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			  	<label>Telp</label>
				<ul>
				<li>
				<label for=""><p style="display:none">Telp</p>
			  	<input type="text" class="textInput medium" name="telp"/>
				</label>
				
				</li>
				<li>
				  <label>Apakah anda ingin telp anda ditampilkan? <input type="checkbox" name="param[tampil_telp]" value="1"/></label>
				  </li>
				</ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			<label>Alamat</label>
			<ul>
			<li>
			  <label for=""><p style="display:none">Alamat</p>
			  	<textarea class="medium required" name="alamat" style="height:5em"></textarea>
			  </label>
			</li>
			<li>
				  <label>Apakah anda ingin alamat anda ditampilkan? 
				  <input type="checkbox" name="param[tampil_alamat]" value="1"/></label>
			    </li>
			</ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
				
			</fieldset>
			<fieldset class="inlineLabels">
			<h4 id='deskripsi'>Deskripsi</h4>
			<script>
			  $(document).ready(function(){
			      $('#kategori').change(function(){
				  $('#deskripsi').html('Isi '+$('#kategori option:selected').text());
			      });
			  });
			</script>
			<div class="ctrlHolder">
			  <label for=""></label>
			  	<textarea name="deskripsi" class="medium required " ></textarea>
			  <p class="formHint">&nbsp;</p>
        	</div>
			
			</fieldset>
			
			<div class="buttonHolder">
        	
        	<button class="primaryAction" type="submit">Kirim</button>
      </div>
</form>
<script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form.jquery.js"></script>
	 <script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form-validation.jquery.js"></script>
	 <script type="text/javascript">
       $(function(){
	   
        $('form.uniForm').uniform({
			
			prevent_submit          : true
        });
      });
       </script>
		<div style="clear: both;"></div><br>
		<?php include('inc/share_button.php');?>
	
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
	  <!-- sidebar end -->
<div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
	$(document).ready(function(){
	    $('#KdSumber').change(function(){
		if($('#KdSumber').val()==9){
		   $('#sumber_lain').show(); 
		}else{
		    $('#sumber_lain').hide();
		}
	    });
	});
</script>
</body>
</html>