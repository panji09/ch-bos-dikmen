<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head_admin.php") ?>
<body>
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<?php require_once('inc/logo.php'); ?>
	<!-- end logo -->
	
	
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<?php require_once('inc/menu_admin.php'); ?>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Berita</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
	
	<?php if($this->uri->segment(3)=='tambah' || $this->uri->segment(3)=='edit'):?>
 	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody><tr valign="top">
	<td>
	
	
		<!--  start step-holder -->
		
		<!--  end step-holder -->
	
		<!-- start id-form -->
		<?php 
			if($this->uri->segment(3)=='tambah')
				$link=site_url('administrator/post_berita/tambah');
			elseif($this->uri->segment(3)=='edit')
				$link=site_url('administrator/post_berita/edit/'.$id);
		?>
		<form action="<?=$link?>" method="post">
		<table cellspacing="0" cellpadding="0" border="0" id="id-form">
		  <tr>
            <th valign="top">Tampil:</th>
		    <td>
				<?php if($this->uri->segment(3)=='edit'):?>
					<input type="checkbox" name="tampil" <?=($berita->tampil=='ya' ? 'checked="checked"' : '')?> value="ya"/>
				<?php elseif($this->uri->segment(3)=='tambah'):?>
					<input type="checkbox" name="tampil" checked="checked" value="ya"/>
				<?php endif;?>
			</td>
		    <td></td>
		    </tr>
		<tbody><tr>
			<th valign="top">Judul:</th>
			<td><input type="text" class="inp-form" name="judul" value="<?=($this->uri->segment(3)=='edit' ? $berita->judul : '')?>" ></td>
			<td></td>
		</tr>
		<tr>
			<th valign="top">Ringkasan:</th>
			<td><textarea class="ckeditor" name="ringkasan"><?=($this->uri->segment(3)=='edit' ? $berita->ringkasan : '')?></textarea></td>
			<td>			</td>
		</tr>
		<tr>
          <th valign="top">Isi:</th>
		  <td><textarea class="ckeditor" name="isi"><?=($this->uri->segment(3)=='edit' ? $berita->isi : '')?></textarea></td>
		  <td></td>
		  </tr>
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" class="form-submit" value="">
			</td>
		<td></td>
	</tr>
	</tbody></table>
	</form>
	<!-- end id-form  -->

	</td>
	
</tr>
<tr>
<td><img width="695" height="1" alt="blank" src="<?=$this->config->item('admin_img')?>/shared/blank.gif"></td>
<td></td>
</tr>
</tbody></table>
	<?php elseif($this->uri->segment(3)=='view'):?>
	<?=$berita->judul?>
	<?=$berita->tanggal?>
	<?=$berita->isi?>
	<?php endif;?>
<div class="clear"></div>
 

</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php require_once('inc/footer.php') ?>
<!-- end footer -->
 
</body>
</html>