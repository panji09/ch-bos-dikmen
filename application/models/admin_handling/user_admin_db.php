<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_admin_db extends CI_Model{
    function __construct(){
        // Call the Model constructor
        parent::__construct();
        
    }
    
    function get($id){
	$query = $this->get_all($filter=array('id' => $id));
	//echo $this->db->last_query();
	if($query->num_rows() == 1){
            return $query->row();
        }else{
            $user_admin_obj = new stdClass();
            foreach($query->list_fields() as $field){
                $user_admin_obj->$field = '';
            }
            return $user_admin_obj;
        }
    }
    function get_all($filter=array(), $limit=null, $offset=0){
	$this->db->select('a.id, a.username, a.password, a.activated, b.nama, b.nip, b.jabatan_dinas, b.jabatan_kjp_bop, b.email, b.alamat_kantor, b.telp_kantor, b.fax_kantor, b.alamat_rumah, b.handphone');
	$this->db->from('user a');
	$this->db->join('person b','a.person_id = b.id', 'left');
	$this->db->where('a.deleted',0);
	
	if($filter){
	    if(isset($filter['id']))
		$this->db->where('a.id', $filter['id']);
	}
	if($limit)
	    $this->db->limit($limit,$offset);
	    
	return $this->db->get();
    }
    function exist($id){
        return $this->db->get_where('user',  array('id' => $id));
    }
    
    function save($id=null, $data_person, $data_user){
        $result=false;
        $CI =& get_instance();
        $CI->load->model('admin_handling/person_db');
        
        $this->db->trans_start();
        
        $exist=$this->exist($id);
        if($exist->num_rows() == 1){
	    //update
	    $user_admin = $exist->row();
	    if($CI->person_db->save($user_admin->person_id,$data_person)){
		//echo $this->db->last_query();
		$this->db->where('id', $id);
		$user_admin_id = $id;
		$result = $this->db->update('handling', $data_user);
	    }
        }else{
	    //insert
	    if($CI->person_db->save($id=null,$data_person)){
		$data_user['person_id'] = $data_person['person_id'];
		if($result=$this->db->insert('user', $data_user)){
		    $user_admin_id = $this->db->insert_id();
		}
	    }
        }
        $this->db->trans_complete();
        
        //echo $this->db->last_query();
        return $result;
    }
    
    function delete($id){
	$this->db->where('id',$id);
	return $this->db->update('user', array('deleted' => 1));
    }
    
    
}
?>