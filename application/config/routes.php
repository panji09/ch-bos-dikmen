<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['admin_handling'] = "admin_handling/admin";
$route['handling'] = 'handling/admin';

//home
$route['home'] = 'home/home';

$route['halaman'] = 'home/halaman';
$route['halaman/(:any)'] = 'home/halaman/$1';

$route['tentang'] = 'home/tentang';
$route['tentang/(:any)'] = 'home/tentang/$1';

$route['kontak'] = 'home/kontak';
$route['kontak/(:any)'] = 'home/kontak/$1';

$route['lihat_pengaduan'] = 'home/lihat_pengaduan';
$route['lihat_pengaduan/(:any)'] = 'home/lihat_pengaduan/$1';

$route['pengaduan'] = 'home/pengaduan';
$route['pengaduan/(:any)'] = 'home/pengaduan/$1';

$route['statistik_pengaduan'] = 'home/statistik_pengaduan';
$route['statistik_pengaduan/(:any)'] = 'home/statistik_pengaduan/$1';

$route['faq'] = 'home/faq';
$route['faq/(:any)'] = 'home/faq/$1';

$route['default_controller'] = "home/home";
$route['404_override'] = '';



/* End of file routes.php */
/* Location: ./application/config/routes.php */