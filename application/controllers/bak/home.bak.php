<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','date'));
		$this->load->model(array('admin_handling/region_db', 'admin_handling/content_dinamis_db','admin_handling/content_statis_db', 'admin_handling/content_files_db', 'admin_handling/pengaduan_db', 'Select_db', 'admin_handling/respon_db'));
		$this->load->library(array('initlib','session','email'));
		$this->load->database();
		session_start();
		$this->provinsi=31;
		
		$this->email_from='notif@disdik.jakarta.go.id';
		$this->email_name='Dinas Pendidikan Provinsi DKI Jakarta';
	}
	
	function pesan($param){
		if($param='registrasi'){
			$this->output->set_output('<p>Untuk bisa lakukan Regristrasi ke <b>Aplikasi Layanan Masyarakat & Penanganan Pengaduan BOS</b>, melalui alamat email dinas/resmi Tim Manajemen BOS Provinsi/Kabupaten/Kota <br>mengirimkan permintaan username dan password kepada Manajemen BOS Pusat di [....alamat sama seperti untuk BOS.....].  Jawaban yang berisi username dan password akan disampaikan ke alamat pemohon</p>');
		}
	}
	public function index(){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Home';
		$data['prov']=$this->provinsi;
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
		
		$data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $offset=0, $limit=5);
		$data['pengumuman']=$this->content_dinamis_db->get_all($content_kategori_id=2, $offset=0, $limit=5);
		$data['peraturan']=$this->content_dinamis_db->get_all($content_kategori_id=3, $offset=0, $limit=5);
		$data['publikasi']=$this->content_dinamis_db->get_all($content_kategori_id=4, $offset=0, $limit=5);
		
		//$data['report']=$this->Select_db->report_kategori('by_status');
		
		$this->load->view('home/home_view',$data);
		
	}
	function post_filter($method){
		$filter=array(
			'provinsi' => '31',
			'kabkota' => $_POST['kabkota'],
			'kecamatan' => $_POST['kecamatan'],
			'cari_teks' => $_POST['cari_teks'],
			'tahun' => $_POST['tahun']
		);
		$this->session->set_userdata($filter);
		
		redirect('home/'.$method);
	}
	function post_filter_hapus($method){
		$filter=array(
			'provinsi' => '31',
			'kabkota' => '',
			'cari_teks' => '',
			'tahun' => ''
		);
		$this->session->unset_userdata($filter);
		redirect('home/'.$method);
	}
	function lihat_pengaduan(){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Pengaduan Online';
		$this->load->library('pagination');
		
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
		$data['kecamatan']=$this->region_db->kecamatan(array('kabkota_id' => $this->session->userdata('kabkota')));
		
		$config['per_page'] = '15';
		$config['uri_segment'] = '3';
		
		
		$config['base_url'] = site_url('home/lihat_pengaduan');
		$config['total_rows'] = $this->pengaduan_db->get_all($filter=array('published' => 1))->num_rows();
		
		$filter=array(
				'provinsi' => $this->provinsi,
				'kabkota' => $this->session->userdata('kabkota'),
				'kecamatan' => $this->session->userdata('kecamatan'),
				'tahun' => $this->session->userdata('tahun'),
				'cari_teks' => $this->session->userdata('cari_teks')
			    );
		
		$data['pengaduan']=$this->pengaduan_db->get_all($filter,$limit=$config['per_page'],$offset=$this->uri->segment($config['uri_segment']));
		$data['num_rows']=$config['total_rows'];
		//config link
		$config['full_tag_open'] = '<div class="pagination" align="center"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '<';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] =  '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		//end config link
		$this->pagination->initialize($config);
		$this->load->view('home/lihat_pengaduan_view',$data);    
		
	}
	function lihat_pengaduan_sms(){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Pengaduan SMS';
		$this->load->library('pagination');
		$data['prov']=$this->Select_db->prov();
		$data['kabkota']=$this->Select_db->kabkota(array('KdProv' => $this->session->userdata('provinsi')));
		
		$config['per_page'] = '15';
		$config['uri_segment'] = '3';
		
		$data_pengaduan=array(
				'provinsi' => $this->session->userdata('provinsi'),
				'kabkota' => $this->session->userdata('kabkota'),
				'tahun' => $this->session->userdata('tahun'),
				'cari_teks' => $this->session->userdata('cari_teks'),
				'offset' => $config['per_page'],
				'limit' => $this->uri->segment($config['uri_segment'])
			    );
		
		$config['base_url'] = site_url('home/lihat_pengaduan');
		$config['total_rows'] = $this->Select_db->pengaduan_sms($data_pengaduan,false)->num_rows();
		
		$data['pengaduan']=$this->Select_db->pengaduan_sms($data_pengaduan,true);
		$data['num_rows']=$config['total_rows'];
		
		//config link
		$config['full_tag_open'] = '<div class="pagination" align="center"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = '<';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] =  '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		//end config link
		
		$this->pagination->initialize($config);
		$this->load->view('home/lihat_pengaduan_sms_view',$data);    
		
	}
	function smspengaduan(){
		$this->load->view('home/smspengaduan_view');
	}
	function chart($chart,$prov,$kabkota,$tahun,$page='home'){
		$this->load->library("domparser");
		$html=$this->domparser->file_get_html('http://bos.kemdiknas.go.id/pengaduan/aplikasi/g3n.php?chart/'.$chart.'&prov='.$prov.'&kab='.$kabkota.'&tahun='.$tahun.'&triwulan=all');
		//if($chart=='r1')
			$find_script=$html->find('script', 0);
		//else
		//	$find_script=$html->find('script', 2);
		
		$script=$find_script->outertext;
		$filter= preg_replace('/\.\/charts\//', $this->config->item('home_chart')."/", $script); //edit url
		if($page=='home')
			$filter= preg_replace('/700/', '500', $filter); //edit width
		else
			$filter= preg_replace('/700/', '700', $filter); //edit width
			
		return $filter;
	}
	function chart_table($chart,$prov,$kabkota,$tahun){
		$this->load->library("domparser");
		$html=$this->domparser->file_get_html('http://bos.kemdiknas.go.id/pengaduan/aplikasi/g3n.php?chart/'.$chart.'&prov='.$prov.'&kab='.$kabkota.'&tahun='.$tahun.'&triwulan=all');
		return $html->find('table', 0);
	}
	function graph($chart=null, $filter=null){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Statistik Pengaduan';
		
		if(!$chart)
			redirect('');
		
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
		/*
		$data['graph']= $this->chart($chart,$prov,$kabkota,$tahun,'graph');
		$data['graph_table']= $this->chart_table($chart,$prov,$kabkota,$tahun);
		*/
		$data_param='';
		$data['daerah']='all';
		if($filter['provinsi']!='all'){
			$data_param['id_prov']=$filter['provinsi'];
			$data['daerah']='provinsi';
		}
		if($filter['kabkota']!='all'){
			$data_param['id_kabkota']=$filter['kabkota'];
			$data['daerah']='kabkota';
		}
		if($filter['tahun']!='all')
			$data_param['tahun']=$filter['tahun'];
		
		if($chart=='r1'){//kategori & status
			//$data['report']=$this->Select_db->report_kategori(null,$data_param);
		}elseif($chart=='r2'){
			//$data['report']=$this->Select_db->report_sumber_info(null,$data_param);
		}elseif($chart=='r3'){
			//$data['report']=$this->Select_db->report_jumlah_status(null,$data_param);
		}elseif($chart=='r4'){
			//$data['report']=$this->Select_db->report_kategori('by_status',$data_param);
		}elseif($chart=='r5'){
			//$data['report']=$this->Select_db->report_pelaku(null,$data_param);
		}elseif($chart=='r6'){
			//$data['report']=$this->Select_db->report_daerah(null,$data_param);
			//echo $this->db->last_query();
		}elseif($chart=='r7'){
			//$data['report']=$this->Select_db->report_media(null,$data_param);
		}
		$this->load->view('home/graph_view',$data);
	
	}
	function post_graph(){
		$chart=$this->input->post('chart');
		$prov=$this->input->post('prov');
		$kabkota=$this->input->post('kabkota');
		$tahun=$this->input->post('tahun');
		$triwulan='all';
		redirect('home/graph/'.$chart.'/'.$prov.'/'.$kabkota.'/'.$tahun);
	}
	function aplikasi(){
		$this->load->view('home/frame_view');
	}
	function pengaduan($param=null){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Pengaduan Online';
		$this->load->view('home/pengaduan_view',$data);
	}
	function faq(){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Frequently Asked Questions (FAQ)';
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
        
		$data['faq_kat']=$this->Select_db->faq_kategori();
		$this->load->view('home/faq_view',$data);
	}
	function berita($param='list',$id=null){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Berita';
		//$data['prov']=$this->Select_db->prov();
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
        
		if($param=='list'){
			$this->load->library('pagination');
			$config['base_url'] = site_url('home/berita/list');
			$config['total_rows'] = $this->content_dinamis_db->get_all($content_kategori_id=1, $limit=null, $offset=null, array('published' => 1))->num_rows();
			$config['per_page'] = '10';
			$config['uri_segment'] = '4';
			
			$data_berita=array(
					'offset' => $config['per_page'],
					'limit' => $this->uri->segment($config['uri_segment'])
				    );
			$data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $limit=$config['per_page'], $offset=$this->uri->segment($config['uri_segment']), array('published' => 1));
			//$data['berita']=$this->content_dinamis_db->get_all($content_kategori_id=1, $limit=1, $offset=0, array('published' => 1));
			$data['num_rows']=$config['total_rows'];
			
			//config link
			$config['full_tag_open'] = '<div class="pagination" align="center"><ul>';
			$config['full_tag_close'] = '</ul></div>';
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] =  '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			//end config link
			
			$this->pagination->initialize($config);
			$this->load->view('home/berita_view',$data);    
		}elseif($param=='view'){
			//$data['id']=$id;
			
			$data['berita']=$this->content_dinamis_db->get($id);
			$data['title']=$data['berita']->title;
			
			$this->load->view('home/berita_view',$data);
		}
        
	}
    function pengumuman($param='list',$id=null){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Pengumuman';
		//$data['prov']=$this->Select_db->prov();
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
        
		if($param=='list'){
			$this->load->library('pagination');
			$config['base_url'] = site_url('home/pengumuman/list');
			$config['total_rows'] = $this->content_dinamis_db->get_all($content_kategori_id=2,  $limit=null, $offset=null, array('published' => 1))->num_rows();
			$config['per_page'] = '10';
			$config['uri_segment'] = '4';
			
			$data_artikel=array(
					'offset' => $config['per_page'],
					'limit' => $this->uri->segment($config['uri_segment'])
				    );
			$data['pengumuman']=$this->content_dinamis_db->get_all($content_kategori_id=2, $limit=$config['per_page'], $offset=$this->uri->segment($config['uri_segment']), array('published' => 1));
			$data['num_rows']=$config['total_rows'];
			
			//config link
			$config['full_tag_open'] = '<div class="pagination" align="center"><ul>';
			$config['full_tag_close'] = '</ul></div>';
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] =  '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			//end config link
			
			$this->pagination->initialize($config);
			$this->load->view('home/pengumuman_view',$data);    
		}elseif($param=='view'){
			$data['pengumuman']=$this->content_dinamis_db->get($id);
			$data['title']=$data['pengumuman']->title;
			
			$this->load->view('home/pengumuman_view',$data);
		}
        
	}
	function peraturan($param='list',$id=null){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Pengumuman';
		//$data['prov']=$this->Select_db->prov();
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
        
		if($param=='list'){
			$this->load->library('pagination');
			$config['base_url'] = site_url('home/peraturan/list');
			$config['total_rows'] = $this->content_dinamis_db->get_all($content_kategori_id=3,  $limit=null, $offset=null, array('published' => 1))->num_rows();
			$config['per_page'] = '10';
			$config['uri_segment'] = '4';
			
			$data_artikel=array(
					'offset' => $config['per_page'],
					'limit' => $this->uri->segment($config['uri_segment'])
				    );
			$data['peraturan']=$this->content_dinamis_db->get_all($content_kategori_id=3, $limit=$config['per_page'], $offset=$this->uri->segment($config['uri_segment']), array('published' => 1));
			$data['num_rows']=$config['total_rows'];
			
			//config link
			$config['full_tag_open'] = '<div class="pagination" align="center"><ul>';
			$config['full_tag_close'] = '</ul></div>';
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] =  '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			//end config link
			
			$this->pagination->initialize($config);
			$this->load->view('home/peraturan_view',$data);    
		}elseif($param=='view'){
			$data['peraturan']=$this->content_dinamis_db->get($id);
			$data['title']=$data['peraturan']->title;
			
			$this->load->view('home/peraturan_view',$data);
		}
	}
	
	function publikasi($param='list',$id=null){
		$data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Pengumuman';
		//$data['prov']=$this->Select_db->prov();
		$data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
        
		if($param=='list'){
			$this->load->library('pagination');
			$config['base_url'] = site_url('home/publikasi/list');
			$config['total_rows'] = $this->content_dinamis_db->get_all($content_kategori_id=4, $limit=null, $offset=null, array('published' => 1))->num_rows();
			$config['per_page'] = '10';
			$config['uri_segment'] = '4';
			
			$data_artikel=array(
					'offset' => $config['per_page'],
					'limit' => $this->uri->segment($config['uri_segment'])
				    );
			$data['publikasi']=$this->content_dinamis_db->get_all($content_kategori_id=4, $limit=$config['per_page'], $offset=$this->uri->segment($config['uri_segment']), array('published' => 1));
			$data['num_rows']=$config['total_rows'];
			
			//config link
			$config['full_tag_open'] = '<div class="pagination" align="center"><ul>';
			$config['full_tag_close'] = '</ul></div>';
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = '<';
			$config['prev_tag_open'] = '<li class="prev">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] =  '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			//end config link
			
			$this->pagination->initialize($config);
			$this->load->view('home/publikasi_view',$data);    
		}elseif($param=='view'){
			$data['publikasi']=$this->content_dinamis_db->get($id);
			$data['title']=$data['publikasi']->title;
			
			$this->load->view('home/publikasi_view',$data);
		}
	}
	function slider(){
		
		$this->initlib->piecemaker_xml();	
	}
	
	function post_pengaduan(){
		//print_r($_POST);
		
		$data_person=array(
		    'nama' => $this->input->post('nama'),
		    'email' => $this->input->post('email'),
		    'handphone' => $this->input->post('telp'),
		    'alamat_rumah' => $this->input->post('alamat')
		);
		
		$data_pengaduan=array(
		    'program_id' => $this->input->post('program'),
		    'kecamatan_id' => $this->input->post('kecamatan'),
		    'kabkota_id' => $this->input->post('kabkota'),
		    'provinsi_id' => $this->provinsi,
		    'jenjang_id' => $this->input->post('jenjang'),
		    'nama_sekolah' => $this->input->post('sekolah'),
		    'tanggal' => date('Y-m-d H:i:s'),
		    'sumber_id' => $this->input->post('sumber_info'),
		    'sumber_lain' => ($this->input->post('sumber_lain')=='' ? null : $this->input->post('sumber_lain')),
		    'kategori_id' => $this->input->post('kategori'),
		    'deskripsi' => $this->input->post('deskripsi'),
		    'last_update' => date('Y-m-d H:i:s'),
		    'media_id' => 1
		);
		
		$param = $this->input->post('param');
		
		$data_param_pengaduan = array(
		    'tampil_nama' => (isset($param['tampil_nama']) ? '1' : '0'),
		    'tampil_telp' => (isset($param['tampil_telp']) ? '1' : '0'),
		    'tampil_alamat' => (isset($param['tampil_alamat']) ? '1' : '0')
		);
		//print_r();
		if($this->pengaduan_db->save($id=null,$data_person, $data_pengaduan, $data_param_pengaduan)){
		    $this->session->set_flashdata('valid', 'true');
		}
		//echo $this->db->last_query();
		redirect('home/pengaduan');
	}
	/*
	function post_pengaduan(){
		
		$data['KdProv']=$this->provinsi;
		$data['KdKab']=$this->input->post('KdKab');
		$data['KdKec']=$this->input->post('KdKec');
		$data['KdJenjang']=$this->input->post('KdJenjang');
		$data['NmSekolah']=$this->input->post('NmSekolah');
		$data['KdKategori']=$this->input->post('KdKategori');
		$data['KetRpKat4']=$this->input->post('KetRpKat4');
		$data['KdSumber']=$this->input->post('KdSumber');
		$data['LainSumber']=$this->input->post('sumber_lain');
		$data['Deskripsi']=$this->input->post('Deskripsi');
		$data['program_id']=$this->input->post('program');
		
		$data['nama']=$this->input->post('nama');
		$data['tampil_nama']=($this->input->post('tampil_nama')=='' ? '0' : '1');
		$data['email']=$this->input->post('email');
		$data['telp']=$this->input->post('telp');
		$data['tampil_telp']=($this->input->post('tampil_telp')=='' ? '0' : '1');
		$data['alamat']=$this->input->post('alamat');
		$data['tampil_alamat']=($this->input->post('tampil_alamat')=='' ? '0' : '1');
		$data['TglKetahui']=date_to_mysqldatetime(now());
		$data['media']=1; //kode pengaduan online
		
		if($this->Insert_db->pengaduan($data)){
			$this->session->set_flashdata('valid', 'true');
			//kirim notifikasi ke email
			$provinsi=$this->Select_db->prov(array('id' =>$data['KdProv']))->row();
			$kabkota=$this->Select_db->kabkota(array('id' =>$data['KdKab']))->row();
			$kecamatan=$this->Select_db->kecamatan(array('id' =>$data['KdKec']))->row();
			$data['lokasi']=($kecamatan->NmKec!='' ? $kecamatan->NmKec.', ' : '').$kabkota->NmKab.', '.$provinsi->NmProv;
			
			//$this->load->view('home/send_email',$data);
			//end kirim notifikasi ke email
			//$this->email->set_newline("\r\n");
			$this->email->from($this->email_from, $this->email_name);
			$this->email->to($data['email']);
			$this->email->subject('P3M disdik DKI Jakarta : Pemberitahuan status Pengaduan');
			
			$msg=$this->load->view('home/send_email',$data,true);
			$this->email->message($msg);
			
			$this->email->send();
					/*
					if($this->email->send())
							echo "terkirim ke ".$data['user']->email;
									//$this->session->set_flashdata('valid',true);
					else
							echo "gagal kirim ke ".$data['user']->email;
									//$this->session->set_flashdata('valid',false);
					*/
		/*
		}
		redirect('home/pengaduan');
	}
	*/
    function about($page=null){
        $data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Tentang BSM';
        
        if($page)
	    $data['tentang']=$this->content_statis_db->get('about_'.$page);
        
        $data['kabkota']=$this->region_db->kabkota(array('provinsi_id' => $this->provinsi));
        
        $this->load->view('home/about_view',$data);
    }
    function gallery(){
        $data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Gallery';
        $data['prov']=$this->Select_db->prov();
	$data['kabkota']=$this->Select_db->kabkota(array('KdProv' => $this->provinsi));
        
	$data['gallery']=$this->Select_db->gallery();
        $this->load->view('home/gallery_view',$data);
    }
    function download(){
        $_SESSION['KCFINDER']=array();
        $_SESSION['KCFINDER']['disabled'] = false;
        $_SESSION['KCFINDER']['access'] =
        array('files' => array(
            'upload' => false,
            'delete' => false,
            'copy' => false,
            'move' => false,
            'rename' => false
        ));
        $_SESSION['KCFINDER']['uploadURL'] = site_url("media/upload");
        $data['title']='Bantuan Siswa Miskin dan Layanan Masyarakat : Unduh Dokumen';
        $data['prov']=$this->provinsi;
		$data['kabkota']=$this->Select_db->kabkota(array('KdProv' => $this->provinsi));
        $this->load->view('home/download_view',$data);
    }
	function testing(){
		$a=1;
		
		
		if($a==1 && $b==2){
			echo 'b';
		}elseif($a==1){
			echo 'a';
		}
		
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */